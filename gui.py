import wx
from bandejaGuiWx import *
import bandejaUtils


class Ventana(MainWindow):
    dicTipos = {"EN_AW_7075": "M", "EN_AW_2024": "S", "CFRP": "C"}
    dicLados = {"cuadrada": "4", "hexagonal": "6"}

    tipo = "M"
    nlados = "4"

    distancia_entre_lados = 100

    def getDEL(self, event):
        dist_entre_lados = self.m_slider_DEL.GetValue()
        # print("DEL: " + str(dist_entre_lados))
        # self.distancia_entre_lados = dist_entre_lados
        self.m_staticText_DEL.SetLabel(str(dist_entre_lados) + " mm")
        bandejaUtils.write_DISTANCIA_ENTRE_LADOS(dist_entre_lados)
        return dist_entre_lados
        # ## event.Skip()

    def getEspesor(self, event):
        espesor = self.m_slider_ESP.GetValue()
        a = bandejaUtils.rango_espesor(self.tipo)[0]
        b = bandejaUtils.rango_espesor(self.tipo)[1]
        espesor = a + (b - a) * espesor / 100.
        # print("Espesor: " + str(espesor))
        self.m_staticText_ESP.SetLabel(
            ("%.3f mm" % (espesor)).replace('.', ','))
        if self.tipo == "C":
            bandejaUtils.write_ESPESOR(espesor / 8.)
        else:
            bandejaUtils.write_ESPESOR(espesor)
        return espesor

    def getMaterial(self, event):
        """Transmite el cambio de material de la interfaz
        al txt y a self.tipo
        """
        material = self.m_choice_Material.GetStringSelection()
        # print("Material: " + material)
        material = material.replace(" ", "_")
        bandejaUtils.write_MATERIAL(material)
        self.tipo = self.dicTipos[material]
        self.setImagen()
        return material

    def getForma(self, event):
        """
        Transmite el cambio de forma de la interfaz
        al txt y a self.nlados
        """
        forma = self.m_choice_Forma.GetStringSelection()
        # print("Forma: " + forma)
        forma = forma.lower()
        bandejaUtils.write_FORMA(forma)
        self.nlados = self.dicLados[forma]
        self.setImagen()
        return forma

    def getMetrica(self, event):
        """
        Transmite el cambio de metrica de la interfaz
        al txt y a self.tipo
        """
        metrica = self.m_choice_Metrica.GetStringSelection()
        # print("Metrica: " + metrica)
        bandejaUtils.write_METRICA(metrica)
        return metrica

    def getLeyes(self, event):
        Leyes = self.m_checkBox_Leyes.GetValue()
        if Leyes:
            leyes = "true"
            self.m_button_ConfAvanz.Disable()
        else:
            leyes = "false"
            self.m_button_ConfAvanz.Enable()
        # print("Leyes: " + leyes)
        bandejaUtils.write_LEYES(leyes)
        return leyes

    def setImagen(self):
        """
        Pone el render del caso activo
        """
        png = u"renders/" + self.tipo + self.nlados + ".png"
        self.m_bitmap_Imagen.SetBitmap(wx.Bitmap(png))

    def launchConfAvanz(self, event):
        """
        Al leer el evento, lanza la ventana
        de Configuracion Avanzada correspondiente
        """
        if self.tipo == "M":
            # frca = Window_ConfAvanzadaM(None)
            frca = ConfiM(None)
            frca.iniciar()
            frca.Show(True)
        elif self.tipo == "S":
            frca = ConfiS(None)
            frca.iniciar()
            frca.Show(True)
        elif self.tipo == "C":
            # frca = Window_ConfAvanzadaC(None)
            # frca.Show(True)
            frca = ConfiC(None)
            frca.iniciar()
            frca.Show(True)

    # def Arranque(self, event):
    def Arranque(self):
        """
        -   Copia las tablas .txt originales
            a un archivo .bak sobre el que se
            trabaja hasta que el usuario le de
            a aplicar.
        -   Lee los parametros iniciales para
            inicializar la informacion de la GUI.
        """
        from shutil import copy
        copy("TablaGeneral.txt", bandejaUtils.filenameGen)
        copy("TablaMec.txt", bandejaUtils.filenameMec)
        copy("TablaSh.txt", bandejaUtils.filenameSh)
        copy("TablaCom.txt", bandejaUtils.filenameCom)
        #
        self.setInitialValues()
        self.setImagen()

    def setInitialValues(self):
        initialValuesGen = bandejaUtils.readDesignTable(
            bandejaUtils.filenameGen)
        initialValuesMec = bandejaUtils.readDesignTable(
            bandejaUtils.filenameMec)
        # initialValuesSh = bandejaUtils.readDesignTable("TablaSh.txt.bak")
        # initialValuesMtC = bandejaUtils.readDesignTable("TablaCom.txt.bak")

        self.m_slider_DEL.SetValue(
            int(initialValuesGen[bandejaUtils.dicParamName["dist_entre_lados"]]))
        self.m_staticText_DEL.SetLabel(
            initialValuesGen[bandejaUtils.dicParamName["dist_entre_lados"]] + " mm")

        espesor = float(initialValuesGen[bandejaUtils.dicParamName["espesor"]])
        if self.tipo == "C":
            espesor /= 8.
        a = bandejaUtils.rango_espesor(self.tipo)[0]
        b = bandejaUtils.rango_espesor(self.tipo)[1]
        self.m_slider_ESP.SetValue(int(100 * (espesor - a) / (b - a)))
        self.m_staticText_ESP.SetLabel(
            ("%.3f mm" % (espesor)).replace('.', ','))

        self.m_choice_Material.SetSelection(bandejaUtils.MATERIAL.index(
            initialValuesGen[bandejaUtils.dicParamName["material"]]))
        self.tipo = self.dicTipos[initialValuesGen[bandejaUtils.dicParamName["material"]]]

        self.m_choice_Forma.SetSelection(bandejaUtils.FORMA.index(
            initialValuesGen[bandejaUtils.dicParamName["forma"]]))
        self.nlados = self.dicLados[initialValuesGen[bandejaUtils.dicParamName["forma"]]]

        self.m_choice_Metrica.SetSelection(bandejaUtils.METRICA.index(
            initialValuesGen[bandejaUtils.dicParamName["metrica"]]))

        self.m_checkBox_Leyes.SetValue(
            bool(initialValuesGen[bandejaUtils.dicParamName["leyesPredet"]]))
        self.m_button_ConfAvanz.Enable() if self.m_checkBox_Leyes.GetValue(
        ) is False else self.m_button_ConfAvanz.Disable()

    def Apply(self, event):
        """
        Copia la informacion de la tabla de trabajo a la
        tabla que lee CATIA
        """
        from shutil import copy
        copy(bandejaUtils.filenameGen, "TablaGeneral.txt")
        copy(bandejaUtils.filenameMec, "TablaMec.txt")
        copy(bandejaUtils.filenameSh, "TablaSh.txt")
        copy(bandejaUtils.filenameCom, "TablaCom.txt")
        # copy("TablaSh.txt.bak", "TablaSh.txt")
        print("---\n VALUES SUCCESFULLY CHANGED\n---")

    def Cancel(self, event):
        """
        Elimina la tabla de trabajo y la vuelve a crear a partir de
        la original
        """
        from shutil import copy
        from os import remove
        remove(bandejaUtils.filenameGen)
        remove(bandejaUtils.filenameMec)
        remove(bandejaUtils.filenameSh)
        remove(bandejaUtils.filenameCom)
        print("Working Design Tables removed")
        copy("TablaGeneral.txt", bandejaUtils.filenameGen)
        copy("TablaMec.txt", bandejaUtils.filenameMec)
        copy("TablaSh.txt", bandejaUtils.filenameSh)
        copy("TablaCom.txt", bandejaUtils.filenameCom)
        print("Working DesignTables.txt regenerated from previous data")


class ConfiC(Window_ConfAvanzadaC):

    def iniciar(self):
        mtcValues = bandejaUtils.readDesignTable(
            bandejaUtils.filenameCom)
        mtcRanges = bandejaUtils.get_mtc_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameCom)

        a1, b1 = mtcRanges[bandejaUtils.dicParamName["mtc_esp_hc"]
                           ][0], mtcRanges[bandejaUtils.dicParamName["mtc_esp_hc"]][1]
        c1 = float(mtcValues[bandejaUtils.dicParamName["mtc_esp_hc"]])
        self.m_slider1.SetValue(100 * (c1 - a1) / (b1 - a1))
        self.m_staticText1.SetLabel(
            (u"Espesor del honeycomb: %.2f mm" % (c1)).replace('.', ','))

        a2, b2 = mtcRanges[bandejaUtils.dicParamName["mtc_pest_largo"]
                           ][0], mtcRanges[bandejaUtils.dicParamName["mtc_pest_largo"]][1]
        c2 = float(mtcValues[bandejaUtils.dicParamName["mtc_pest_largo"]])
        self.m_slider2.SetValue(100 * (c2 - a2) / (b2 - a2))
        self.m_staticText2.SetLabel(
            (u"Largo de las pestañas: %.2f mm" % (c2)).replace('.', ','))

        a3, b3 = mtcRanges[bandejaUtils.dicParamName["mtc_pest_ancho"]
                           ][0], mtcRanges[bandejaUtils.dicParamName["mtc_pest_ancho"]][1]
        c3 = float(mtcValues[bandejaUtils.dicParamName["mtc_pest_ancho"]])
        self.m_slider3.SetValue(100 * (c3 - a3) / (b3 - a3))
        self.m_staticText3.SetLabel(
            (u"Ancho de las pestañas: %.2f mm" % (c3)).replace('.', ','))

    def getMtCValue1(self, event):
        # mtcValues = bandejaUtils.readDesignTable(bandejaUtils.filenameCom)
        mtcRanges = bandejaUtils.get_mtc_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameCom)
        # print("entra1")
        a, b = mtcRanges[bandejaUtils.dicParamName["mtc_esp_hc"]
                         ][0], mtcRanges[bandejaUtils.dicParamName["mtc_esp_hc"]][1]
        # c1 = float(mtcValues[bandejaUtils.dicParamName["mtc_esp_hc"]])
        c = self.m_slider1.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText1.SetLabel(
            (u"Espesor del honeycomb: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameCom, "mtc_esp_hc", c)

    def getMtCValue2(self, event):
        # mtcValues = bandejaUtils.readDesignTable(bandejaUtils.filenameCom)
        mtcRanges = bandejaUtils.get_mtc_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameCom)
        # print("entra2")
        a, b = mtcRanges[bandejaUtils.dicParamName["mtc_pest_largo"]
                         ][0], mtcRanges[bandejaUtils.dicParamName["mtc_pest_largo"]][1]
        c = self.m_slider2.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText2.SetLabel(
            (u"Largo de las pestañas: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameCom, "mtc_pest_largo", c)

    def getMtCValue3(self, event):
        # mtcValues = bandejaUtils.readDesignTable(bandejaUtils.filenameCom)
        mtcRanges = bandejaUtils.get_mtc_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameCom)
        # print("entra3")
        a, b = mtcRanges[bandejaUtils.dicParamName["mtc_pest_ancho"]
                         ][0], mtcRanges[bandejaUtils.dicParamName["mtc_pest_ancho"]][1]
        # c1 = float(mtcValues[bandejaUtils.dicParamName["mtc_esp_hc"]])
        c = self.m_slider3.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText3.SetLabel(
            (u"Ancho de las pestañas: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameCom, "mtc_pest_largo", c)


class ConfiS(Window_ConfAvanzadaS):
    def iniciar(self):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameSh)
        Ranges = bandejaUtils.get_sh_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameSh)

        a, b = Ranges[bandejaUtils.dicParamName["sh_doblado"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_doblado"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_doblado"]])
        self.m_slider1.SetValue(100 * (c - a) / (b - a))
        self.m_staticText1.SetLabel(
            (u"Radio de doblado: %.2f mm" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["sh_ext"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_ext"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_ext"]])
        self.m_slider2.SetValue(100 * (c - a) / (b - a))
        self.m_staticText2.SetLabel(
            (u"Ancho del anillo externo: %.2f mm" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["sh_rda_int"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_rda_int"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_rda_int"]])
        self.m_slider3.SetValue(100 * (c - a) / (b - a))
        self.m_staticText3.SetLabel(
            (u"Radios de acuerdo: %.2f mm" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["sh_nervios"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_nervios"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_nervios"]])
        self.m_slider4.SetValue(100 * (c - a) / (b - a))
        self.m_staticText4.SetLabel(
            (u"Ancho de los nervios: %.2f mm" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["sh_r_circ_int"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_r_circ_int"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_r_circ_int"]])
        self.m_slider5.SetValue(100 * (c - a) / (b - a))
        self.m_staticText5.SetLabel(
            (u"Radio del anillo interior: %.2f mm" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["sh_soporte"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_soporte"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_soporte"]])
        self.m_slider6.SetValue(100 * (c - a) / (b - a))
        self.m_staticText6.SetLabel(
            (u"Lado de los soportes de las esquinas: %.2f mm" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["sh_pestanha"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_pestanha"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_pestanha"]])
        self.m_slider7.SetValue(100 * (c - a) / (b - a))
        self.m_staticText7.SetLabel(
            (u"Ancho de las pestañas: %.2f mm" % (c)).replace('.', ','))

    def get_val_1(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameSh)
        Ranges = bandejaUtils.get_sh_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameSh)

        a, b = Ranges[bandejaUtils.dicParamName["sh_doblado"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_doblado"]][1]
        # c = float(Values[bandejaUtils.dicParamName["sh_doblado"]])
        c = self.m_slider1.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText1.SetLabel(
            (u"Radio de doblado: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameSh, "sh_doblado", c)

    def get_val_2(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameSh)
        Ranges = bandejaUtils.get_sh_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameSh)
        a, b = Ranges[bandejaUtils.dicParamName["sh_ext"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_ext"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_ext"]])
        c = self.m_slider2.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText2.SetLabel(
            (u"Ancho del anillo externo: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(bandejaUtils.filenameSh, "sh_ext", c)

    def get_val_3(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameSh)
        Ranges = bandejaUtils.get_sh_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameSh)
        a, b = Ranges[bandejaUtils.dicParamName["sh_rda_int"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_rda_int"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_rda_int"]])
        c = self.m_slider3.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText3.SetLabel(
            (u"Radios de acuerdo: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameSh, "sh_rda_int", c)

    def get_val_4(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameSh)
        Ranges = bandejaUtils.get_sh_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameSh)
        a, b = Ranges[bandejaUtils.dicParamName["sh_nervios"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_nervios"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_nervios"]])
        c = self.m_slider4.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText4.SetLabel(
            (u"Ancho de los nervios: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameSh, "sh_nervios", c)

    def get_val_5(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameSh)
        Ranges = bandejaUtils.get_sh_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameSh)
        a, b = Ranges[bandejaUtils.dicParamName["sh_r_circ_int"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_r_circ_int"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_r_circ_int"]])
        c = self.m_slider5.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText5.SetLabel(
            (u"Radio del anillo interior: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameSh, "sh_r_circ_int", c)

    def get_val_6(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameSh)
        Ranges = bandejaUtils.get_sh_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameSh)
        a, b = Ranges[bandejaUtils.dicParamName["sh_soporte"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_soporte"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_soporte"]])
        c = self.m_slider6.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText6.SetLabel(
            (u"Lado de los soportes de las esquinas: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameSh, "sh_soporte", c)

    def get_val_7(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameSh)
        Ranges = bandejaUtils.get_sh_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameSh)
        a, b = Ranges[bandejaUtils.dicParamName["sh_pestanha"]
                      ][0], Ranges[bandejaUtils.dicParamName["sh_pestanha"]][1]
        c = float(Values[bandejaUtils.dicParamName["sh_pestanha"]])
        c = self.m_slider7.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText7.SetLabel(
            (u"Ancho de las pestañas: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameSh, "sh_pestanha", c)


class ConfiM(Window_ConfAvanzadaM):
    def iniciar(self):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameMec)
        Ranges = bandejaUtils.get_mec_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameMec)

        a, b = Ranges[bandejaUtils.dicParamName["mec_nervios"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_nervios"]][1]
        c = float(Values[bandejaUtils.dicParamName["mec_nervios"]])
        self.m_slider1.SetValue(100 * (c - a) / (b - a))
        self.m_staticText1.SetLabel(
            (u"Ancho de los nervios: %.2f mm" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["mec_ext"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_ext"]][1]
        c = float(Values[bandejaUtils.dicParamName["mec_ext"]])
        self.m_slider2.SetValue(100 * (c - a) / (b - a))
        self.m_staticText2.SetLabel(
            (u"Ancho del anillo externo: %.2f mm" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["mec_int_long"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_int_long"]][1]
        c = float(Values[bandejaUtils.dicParamName["mec_int_long"]])
        self.m_slider3.SetValue(100 * (c - a) / (b - a))
        self.m_staticText3.SetLabel(
            (u"Longitud característica de la estructura central: %.2f mm" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["mec_rda_int"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_rda_int"]][1]
        c = float(Values[bandejaUtils.dicParamName["mec_rda_int"]])
        self.m_slider4.SetValue(100 * (c - a) / (b - a))
        self.m_staticText4.SetLabel(
            (u"Radios de acuerdo: %.2f mm" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["mec_prof"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_prof"]][1]
        c = float(Values[bandejaUtils.dicParamName["mec_prof"]])
        self.m_slider5.SetValue(100 * (c - a) / (b - a))
        self.m_staticText5.SetLabel(
            (u"Profundidad de mecanizado: %.2f mm" % (c)).replace('.', ','))

        # a, b = Ranges[bandejaUtils.dicParamName["mec_n_grid"]
        #               ][0], Ranges[bandejaUtils.dicParamName["mec_n_grid"]][1]
        c = int(Values[bandejaUtils.dicParamName["mec_n_grid"]])
        self.m_slider6.SetValue(int(c))
        self.m_staticText6.SetLabel(
            (u"[forma cuadrada] Número de vaciados por lado: %i" % (c)).replace('.', ','))

        a, b = Ranges[bandejaUtils.dicParamName["mec_pletina"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_pletina"]][1]
        c = float(Values[bandejaUtils.dicParamName["mec_pletina"]])
        self.m_slider7.SetValue(100 * (c - a) / (b - a))
        self.m_staticText7.SetLabel(
            (u"Ancho de los alojamientos para las pletinas: %.2f mm" % (c)).replace('.', ','))

    def get_val_1(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameMec)
        Ranges = bandejaUtils.get_mec_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameMec)

        a, b = Ranges[bandejaUtils.dicParamName["mec_nervios"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_nervios"]][1]
        c = self.m_slider1.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText1.SetLabel(
            (u"Ancho de los nervios: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameMec, "mec_nervios", c)

    def get_val_2(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameMec)
        Ranges = bandejaUtils.get_mec_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameMec)
        a, b = Ranges[bandejaUtils.dicParamName["mec_ext"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_ext"]][1]
        c = self.m_slider2.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText2.SetLabel(
            (u"Ancho del anillo externo: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(bandejaUtils.filenameMec, "mec_ext", c)

    def get_val_3(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameMec)
        Ranges = bandejaUtils.get_mec_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameMec)
        a, b = Ranges[bandejaUtils.dicParamName["mec_int_long"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_int_long"]][1]
        c = self.m_slider3.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText3.SetLabel(
            (u"Longitud característica de la estructura central: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameMec, "mec_int_long", c)

    def get_val_4(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameMec)
        Ranges = bandejaUtils.get_mec_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameMec)
        a, b = Ranges[bandejaUtils.dicParamName["mec_rda_int"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_rda_int"]][1]
        c = self.m_slider4.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText4.SetLabel(
            (u"Radios de acuerdo: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameMec, "mec_rda_int", c)

    def get_val_5(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameMec)
        Ranges = bandejaUtils.get_mec_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameMec)
        a, b = Ranges[bandejaUtils.dicParamName["mec_prof"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_prof"]][1]
        c = self.m_slider5.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText5.SetLabel(
            (u"Profundidad de mecanizado: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameMec, "mec_prof", c)

    def get_val_6(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameMec)
        Ranges = bandejaUtils.get_mec_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameMec)
        # a, b = Ranges[bandejaUtils.dicParamName["mec_n_grid"]
        #               ][0], Ranges[bandejaUtils.dicParamName["mec_n_grid"]][1]
        c = self.m_slider6.GetValue()
        self.m_staticText6.SetLabel(
            (u"[forma cuadrada] Número de vaciados por lado: %i" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameMec, "mec_n_grid", c)

    def get_val_7(self, event):
        Values = bandejaUtils.readDesignTable(
            bandejaUtils.filenameMec)
        Ranges = bandejaUtils.get_mec_ranges(
            bandejaUtils.filenameGen, bandejaUtils.filenameMec)
        a, b = Ranges[bandejaUtils.dicParamName["mec_pletina"]
                      ][0], Ranges[bandejaUtils.dicParamName["mec_pletina"]][1]
        c = self.m_slider7.GetValue()
        c = a + (b - a) * c / 100.
        self.m_staticText7.SetLabel(
            (u"Ancho de los alojamientos para las pletinas: %.2f mm" % (c)).replace('.', ','))
        bandejaUtils.changeValueInFile(
            bandejaUtils.filenameMec, "mec_pletina", c)


if __name__ == '__main__':
    app = wx.App()
    fr = Ventana(None)
    fr.Arranque()
    fr.Show(True)

    app.MainLoop()
