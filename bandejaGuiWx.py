# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Dec 21 2016)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainWindow
###########################################################################

class MainWindow ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Configurador de bandeja", pos = wx.DefaultPosition, size = wx.Size( 920,374 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.Size( 920,374 ), wx.Size( 920,374 ) )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer13 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer13.SetMinSize( wx.Size( 900,300 ) ) 
		sbSizer_SelecPar = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Selección de parámetros" ), wx.VERTICAL )
		
		sbSizer_SelecPar.SetMinSize( wx.Size( 500,300 ) ) 
		self.m_staticText241 = wx.StaticText( sbSizer_SelecPar.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText241.Wrap( -1 )
		sbSizer_SelecPar.Add( self.m_staticText241, 0, wx.ALL, 5 )
		
		bSizer_Longitudes = wx.BoxSizer( wx.HORIZONTAL )
		
		sbSizer_DEL = wx.StaticBoxSizer( wx.StaticBox( sbSizer_SelecPar.GetStaticBox(), wx.ID_ANY, u"Distancia entre lados" ), wx.VERTICAL )
		
		sbSizer_DEL.SetMinSize( wx.Size( -1,50 ) ) 
		self.m_slider_DEL = wx.Slider( sbSizer_DEL.GetStaticBox(), wx.ID_ANY, 200, 80, 500, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		sbSizer_DEL.Add( self.m_slider_DEL, 0, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText_DEL = wx.StaticText( sbSizer_DEL.GetStaticBox(), wx.ID_ANY, u"mm", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText_DEL.Wrap( -1 )
		sbSizer_DEL.Add( self.m_staticText_DEL, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		
		bSizer_Longitudes.Add( sbSizer_DEL, 1, wx.EXPAND, 5 )
		
		sbSizer_ESP = wx.StaticBoxSizer( wx.StaticBox( sbSizer_SelecPar.GetStaticBox(), wx.ID_ANY, u"Espesor" ), wx.VERTICAL )
		
		self.m_slider_ESP = wx.Slider( sbSizer_ESP.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		sbSizer_ESP.Add( self.m_slider_ESP, 0, wx.ALIGN_CENTER|wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText_ESP = wx.StaticText( sbSizer_ESP.GetStaticBox(), wx.ID_ANY, u"mm", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText_ESP.Wrap( -1 )
		sbSizer_ESP.Add( self.m_staticText_ESP, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		
		bSizer_Longitudes.Add( sbSizer_ESP, 1, wx.EXPAND, 5 )
		
		
		sbSizer_SelecPar.Add( bSizer_Longitudes, 1, wx.EXPAND, 5 )
		
		self.m_staticText25 = wx.StaticText( sbSizer_SelecPar.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText25.Wrap( -1 )
		sbSizer_SelecPar.Add( self.m_staticText25, 0, wx.ALL, 5 )
		
		bSizer_MFM = wx.BoxSizer( wx.HORIZONTAL )
		
		sbSizer_Material = wx.StaticBoxSizer( wx.StaticBox( sbSizer_SelecPar.GetStaticBox(), wx.ID_ANY, u"Material" ), wx.VERTICAL )
		
		m_choice_MaterialChoices = [ u"EN AW 7075", u"EN AW 2024", u"CFRP" ]
		self.m_choice_Material = wx.Choice( sbSizer_Material.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice_MaterialChoices, 0 )
		self.m_choice_Material.SetSelection( 0 )
		sbSizer_Material.Add( self.m_choice_Material, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		bSizer_MFM.Add( sbSizer_Material, 1, wx.EXPAND, 5 )
		
		sbSizer_Forma = wx.StaticBoxSizer( wx.StaticBox( sbSizer_SelecPar.GetStaticBox(), wx.ID_ANY, u"Forma" ), wx.VERTICAL )
		
		m_choice_FormaChoices = [ u"Cuadrada", u"Hexagonal" ]
		self.m_choice_Forma = wx.Choice( sbSizer_Forma.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice_FormaChoices, 0 )
		self.m_choice_Forma.SetSelection( 0 )
		sbSizer_Forma.Add( self.m_choice_Forma, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		bSizer_MFM.Add( sbSizer_Forma, 1, wx.EXPAND, 5 )
		
		sbSizer_Metrica = wx.StaticBoxSizer( wx.StaticBox( sbSizer_SelecPar.GetStaticBox(), wx.ID_ANY, u"Métrica" ), wx.VERTICAL )
		
		m_choice_MetricaChoices = [ u"M2", u"M3", u"M4" ]
		self.m_choice_Metrica = wx.Choice( sbSizer_Metrica.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice_MetricaChoices, 0 )
		self.m_choice_Metrica.SetSelection( 2 )
		sbSizer_Metrica.Add( self.m_choice_Metrica, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		bSizer_MFM.Add( sbSizer_Metrica, 1, wx.EXPAND, 5 )
		
		
		sbSizer_SelecPar.Add( bSizer_MFM, 1, wx.EXPAND|wx.TOP, 5 )
		
		self.m_staticText26 = wx.StaticText( sbSizer_SelecPar.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText26.Wrap( -1 )
		sbSizer_SelecPar.Add( self.m_staticText26, 0, wx.ALL, 5 )
		
		bSizer_Extra = wx.BoxSizer( wx.VERTICAL )
		
		bSizer_Conf = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer_Leyes = wx.BoxSizer( wx.VERTICAL )
		
		self.m_checkBox_Leyes = wx.CheckBox( sbSizer_SelecPar.GetStaticBox(), wx.ID_ANY, u"Aplicar las leyes predeterminadas \nincluidas en el modelo.", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer_Leyes.Add( self.m_checkBox_Leyes, 0, wx.ALL, 5 )
		
		
		bSizer_Conf.Add( bSizer_Leyes, 1, wx.EXPAND, 5 )
		
		bSizer20 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_button_ConfAvanz = wx.Button( sbSizer_SelecPar.GetStaticBox(), wx.ID_ANY, u"Configuración avanzada", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer20.Add( self.m_button_ConfAvanz, 0, wx.ALIGN_RIGHT|wx.ALL, 5 )
		
		
		bSizer_Conf.Add( bSizer20, 1, wx.EXPAND, 5 )
		
		
		bSizer_Extra.Add( bSizer_Conf, 1, wx.EXPAND, 5 )
		
		m_sdbSizer_ApplyCancel = wx.StdDialogButtonSizer()
		self.m_sdbSizer_ApplyCancelApply = wx.Button( sbSizer_SelecPar.GetStaticBox(), wx.ID_APPLY )
		m_sdbSizer_ApplyCancel.AddButton( self.m_sdbSizer_ApplyCancelApply )
		self.m_sdbSizer_ApplyCancelCancel = wx.Button( sbSizer_SelecPar.GetStaticBox(), wx.ID_CANCEL )
		m_sdbSizer_ApplyCancel.AddButton( self.m_sdbSizer_ApplyCancelCancel )
		m_sdbSizer_ApplyCancel.Realize();
		
		bSizer_Extra.Add( m_sdbSizer_ApplyCancel, 1, wx.EXPAND, 5 )
		
		
		sbSizer_SelecPar.Add( bSizer_Extra, 1, wx.EXPAND|wx.TOP, 5 )
		
		
		bSizer13.Add( sbSizer_SelecPar, 1, wx.EXPAND|wx.FIXED_MINSIZE|wx.TOP, 5 )
		
		sbSizer_Preview = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Vista previa" ), wx.VERTICAL )
		
		sbSizer_Preview.SetMinSize( wx.Size( 415,300 ) ) 
		self.m_bitmap_Imagen = wx.StaticBitmap( sbSizer_Preview.GetStaticBox(), wx.ID_ANY, wx.Bitmap( u"renders/M4.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.Size( 375,281 ), 0 )
		self.m_bitmap_Imagen.SetMinSize( wx.Size( 375,281 ) )
		self.m_bitmap_Imagen.SetMaxSize( wx.Size( 375,281 ) )
		
		sbSizer_Preview.Add( self.m_bitmap_Imagen, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
		
		
		bSizer13.Add( sbSizer_Preview, 1, wx.EXPAND|wx.TOP, 5 )
		
		
		bSizer2.Add( bSizer13, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer2 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_slider_DEL.Bind( wx.EVT_SCROLL, self.getDEL )
		self.m_slider_ESP.Bind( wx.EVT_SCROLL, self.getEspesor )
		self.m_choice_Material.Bind( wx.EVT_CHOICE, self.getMaterial )
		self.m_choice_Forma.Bind( wx.EVT_CHOICE, self.getForma )
		self.m_choice_Metrica.Bind( wx.EVT_CHOICE, self.getMetrica )
		self.m_checkBox_Leyes.Bind( wx.EVT_CHECKBOX, self.getLeyes )
		self.m_button_ConfAvanz.Bind( wx.EVT_BUTTON, self.launchConfAvanz )
		self.m_sdbSizer_ApplyCancelApply.Bind( wx.EVT_BUTTON, self.Apply )
		self.m_sdbSizer_ApplyCancelCancel.Bind( wx.EVT_BUTTON, self.Cancel )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def getDEL( self, event ):
		event.Skip()
	
	def getEspesor( self, event ):
		event.Skip()
	
	def getMaterial( self, event ):
		event.Skip()
	
	def getForma( self, event ):
		event.Skip()
	
	def getMetrica( self, event ):
		event.Skip()
	
	def getLeyes( self, event ):
		event.Skip()
	
	def launchConfAvanz( self, event ):
		event.Skip()
	
	def Apply( self, event ):
		event.Skip()
	
	def Cancel( self, event ):
		event.Skip()
	

###########################################################################
## Class Window_ConfAvanzadaM
###########################################################################

class Window_ConfAvanzadaM ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Configuración avanzada", pos = wx.DefaultPosition, size = wx.Size( 700,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.Size( 700,300 ), wx.Size( 700,300 ) )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Mecanizado: EN AW 7075" ), wx.HORIZONTAL )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText1 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Ancho de los nervios (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText1.Wrap( -1 )
		bSizer6.Add( self.m_staticText1, 0, wx.ALL, 5 )
		
		self.m_staticText2 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Ancho del anillo externo (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText2.Wrap( -1 )
		bSizer6.Add( self.m_staticText2, 0, wx.ALL, 5 )
		
		self.m_staticText3 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Longitud característica de la estructura central (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText3.Wrap( -1 )
		bSizer6.Add( self.m_staticText3, 0, wx.ALL, 5 )
		
		self.m_staticText4 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Radios de acuerdo (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText4.Wrap( -1 )
		bSizer6.Add( self.m_staticText4, 0, wx.ALL, 5 )
		
		self.m_staticText5 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Profundidad de mecanizado (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText5.Wrap( -1 )
		bSizer6.Add( self.m_staticText5, 0, wx.ALL, 5 )
		
		self.m_staticText6 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"[forma cuadrada] Número de vaciados por lado", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText6.Wrap( -1 )
		bSizer6.Add( self.m_staticText6, 0, wx.ALL, 5 )
		
		self.m_staticText7 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Ancho de los alojamientos para las pletinas (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText7.Wrap( -1 )
		bSizer6.Add( self.m_staticText7, 0, wx.ALL, 5 )
		
		
		sbSizer3.Add( bSizer6, 1, wx.EXPAND, 5 )
		
		bSizer7 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_slider1 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider1, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider2 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider2, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider3 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider3, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider4 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider4, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider5 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider5, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider6 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 4, 1, 8, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider6, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider7 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider7, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		sbSizer3.Add( bSizer7, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( sbSizer3 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_slider1.Bind( wx.EVT_SCROLL, self.get_val_1 )
		self.m_slider2.Bind( wx.EVT_SCROLL, self.get_val_2 )
		self.m_slider3.Bind( wx.EVT_SCROLL, self.get_val_3 )
		self.m_slider4.Bind( wx.EVT_SCROLL, self.get_val_4 )
		self.m_slider5.Bind( wx.EVT_SCROLL, self.get_val_5 )
		self.m_slider6.Bind( wx.EVT_SCROLL, self.get_val_6 )
		self.m_slider7.Bind( wx.EVT_SCROLL, self.get_val_7 )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def get_val_1( self, event ):
		event.Skip()
	
	def get_val_2( self, event ):
		event.Skip()
	
	def get_val_3( self, event ):
		event.Skip()
	
	def get_val_4( self, event ):
		event.Skip()
	
	def get_val_5( self, event ):
		event.Skip()
	
	def get_val_6( self, event ):
		event.Skip()
	
	def get_val_7( self, event ):
		event.Skip()
	

###########################################################################
## Class Window_ConfAvanzadaS
###########################################################################

class Window_ConfAvanzadaS ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Configuración avanzada", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Chapa: EN AW 2024" ), wx.HORIZONTAL )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText1 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Radio de doblado (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText1.Wrap( -1 )
		bSizer6.Add( self.m_staticText1, 0, wx.ALL, 5 )
		
		self.m_staticText2 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Ancho del anillo externo (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText2.Wrap( -1 )
		bSizer6.Add( self.m_staticText2, 0, wx.ALL, 5 )
		
		self.m_staticText3 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Radios de acuerdo (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText3.Wrap( -1 )
		bSizer6.Add( self.m_staticText3, 0, wx.ALL, 5 )
		
		self.m_staticText4 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Ancho de los nervios (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText4.Wrap( -1 )
		bSizer6.Add( self.m_staticText4, 0, wx.ALL, 5 )
		
		self.m_staticText5 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Radio del anillo interior (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText5.Wrap( -1 )
		bSizer6.Add( self.m_staticText5, 0, wx.ALL, 5 )
		
		self.m_staticText6 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Lado de los soportes de las esquinas (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText6.Wrap( -1 )
		bSizer6.Add( self.m_staticText6, 0, wx.ALL, 5 )
		
		self.m_staticText7 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Ancho de las pestañas (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText7.Wrap( -1 )
		bSizer6.Add( self.m_staticText7, 0, wx.ALL, 5 )
		
		
		sbSizer3.Add( bSizer6, 1, wx.EXPAND, 5 )
		
		bSizer7 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_slider1 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider1, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider2 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider2, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider3 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider3, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider4 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider4, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider5 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider5, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider6 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider6, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider7 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider7, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		sbSizer3.Add( bSizer7, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( sbSizer3 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.m_slider1.Bind( wx.EVT_SCROLL, self.get_val_1 )
		self.m_slider2.Bind( wx.EVT_SCROLL, self.get_val_2 )
		self.m_slider3.Bind( wx.EVT_SCROLL, self.get_val_3 )
		self.m_slider4.Bind( wx.EVT_SCROLL, self.get_val_4 )
		self.m_slider5.Bind( wx.EVT_SCROLL, self.get_val_5 )
		self.m_slider6.Bind( wx.EVT_SCROLL, self.get_val_6 )
		self.m_slider7.Bind( wx.EVT_SCROLL, self.get_val_7 )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def get_val_1( self, event ):
		event.Skip()
	
	def get_val_2( self, event ):
		event.Skip()
	
	def get_val_3( self, event ):
		event.Skip()
	
	def get_val_4( self, event ):
		event.Skip()
	
	def get_val_5( self, event ):
		event.Skip()
	
	def get_val_6( self, event ):
		event.Skip()
	
	def get_val_7( self, event ):
		event.Skip()
	

###########################################################################
## Class Window_ConfAvanzadaC
###########################################################################

class Window_ConfAvanzadaC ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Configuración avanzada", pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"Material compuesto: CFRP" ), wx.HORIZONTAL )
		
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText1 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Espesor del honeycomb (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText1.Wrap( -1 )
		bSizer6.Add( self.m_staticText1, 0, wx.ALL, 5 )
		
		self.m_staticText2 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Largo de las pestañas (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText2.Wrap( -1 )
		bSizer6.Add( self.m_staticText2, 0, wx.ALL, 5 )
		
		self.m_staticText3 = wx.StaticText( sbSizer3.GetStaticBox(), wx.ID_ANY, u"Ancho de las pestañas (mm)", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText3.Wrap( -1 )
		bSizer6.Add( self.m_staticText3, 0, wx.ALL, 5 )
		
		
		sbSizer3.Add( bSizer6, 1, wx.EXPAND, 5 )
		
		bSizer7 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_slider1 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider1, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider2 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider2, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_slider3 = wx.Slider( sbSizer3.GetStaticBox(), wx.ID_ANY, 50, 0, 100, wx.DefaultPosition, wx.DefaultSize, wx.SL_BOTH|wx.SL_HORIZONTAL )
		bSizer7.Add( self.m_slider3, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		sbSizer3.Add( bSizer7, 1, wx.EXPAND, 5 )
		
		
		self.SetSizer( sbSizer3 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.getMtCValue )
		self.m_slider1.Bind( wx.EVT_SCROLL, self.getMtCValue1 )
		self.m_slider2.Bind( wx.EVT_SCROLL, self.getMtCValue2 )
		self.m_slider3.Bind( wx.EVT_SCROLL, self.getMtCValue3 )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def getMtCValue( self, event ):
		event.Skip()
	
	def getMtCValue1( self, event ):
		event.Skip()
	
	def getMtCValue2( self, event ):
		event.Skip()
	
	def getMtCValue3( self, event ):
		event.Skip()
	

