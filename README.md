# DISEÑO PARAMETRIZADO DE BANDEJA DE MICROSATÉLITE

**Autor:** Daniel Lubián Arenillas

**Fecha:** 2017-11-20

## CATIA

* `PARAMETERS.CATPart`: 

* `base.CATPart`: 

* `CATalog.CATMaterial`:

## GUI

* `gui.py`

* `bandejaUtils.py`
* `bandejaGuiWx.py`
* `gui.fbp`
* `compyle.bat`

### Instrucciones de ejecución y compilación

Paquetes requeridos: 
* `python` (3.6)
* `wxPython` (4.00b2)

Compilación a `exe`:

Requiere `pyinstaller`

`compyle.bat`