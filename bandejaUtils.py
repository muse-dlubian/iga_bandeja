import math

filenameGen = "TablaGeneral.txt.bak"
filenameMec = "TablaMec.txt.bak"
filenameSh = "TablaSh.txt.bak"
filenameCom = "TablaCom.txt.bak"

FORMA       = ["cuadrada", "hexagonal"]
MATERIAL    = ["EN_AW_7075", "EN_AW_2024", "CFRP"]
METRICA     = ["M2", "M3", "M4"]
LEYES       = [False, True]

RANGO_ESPESOR_M = [2, 10]
RANGO_ESPESOR_S = [0.5, 3]
RANGO_ESPESOR_C = [5, 20]

dicParamName = {
    "dist_entre_lados": r"`DISTANCIA_ENTRE_LADOS` (mm)",
    "espesor":          r"`ESPESOR` (mm)",
    "forma":            r"`FORMA`",
    "material":         r"`MATERIAL`",
    "metrica":          r"`METRICA_ROSCA`",
    "leyesPredet":      r"`LEYES`",
    "mec_espesor":      r"`Set_Mecanizado\M__ESPESOR` (mm)",
    "mec_nervios":      r"`Set_Mecanizado\M__ANCHO_NERVIOS` (mm)",
    "mec_ext":          r"`Set_Mecanizado\M__ANCHO_ANILLO_EXTERNO` (mm)",
    "mec_int_long":     r"`Set_Mecanizado\M__CENTRO_DIST_LADOS` (mm)",
    "mec_rda_int":      r"`Set_Mecanizado\M__RADIO_DE_ACUERDO_INTERNO` (mm)",
    "mec_prof":         r"`Set_Mecanizado\M__PROF_MECANIZADO` (mm)",
    "mec_n_grid":       r"`Set_Mecanizado\M___NUM_GRID_VACIADOS_LADO`",
    "mec_pletina":      r"`Set_Mecanizado\M___ANCHO_PLETINA` (mm)",
    "sh_doblado":       r"`Set_Chapa\S__RADIO_DOBLADO` (mm)",
    "sh_espesor":       r"`Set_Chapa\S__ESPESOR` (mm)",
    "sh_ext":           r"`Set_Chapa\S__ANCHO_ANILLO_EXTERNO` (mm)",
    "sh_rda_int":       r"`Set_Chapa\S__RADIO_DE_ACUERDO_INTERNO` (mm)",
    "sh_nervios":       r"`Set_Chapa\S__ANCHO_NERVIOS` (mm)",
    "sh_r_circ_int":    r"`Set_Chapa\S__RADIO_CIRCULO_INT` (mm)",
    "sh_soporte":       r"`Set_Chapa\S__LADO_CUADRADOS_SOPORTE` (mm)",
    "sh_pestanha":      r"`Set_Chapa\S__ANCHO_PESTANHA` (mm)",
    "mtc_esp_hc":       r"`Set_MatComp\C__ESPESOR_HONEYCOMB` (mm)",
    "mtc_pest_largo":   r"`Set_MatComp\C__PESTANHA_LARGO` (mm)",
    "mtc_pest_ancho":   r"`Set_MatComp\C__PESTANHA_ANCHO` (mm)"
}

dicLine = {
    "dist_entre_lados": 0,
    "espesor":          1,
    "forma":            2,
    "material":         3,
    "metrica":          4,
    "leyesPredet":      5,
    "mec_nervios":      0,
    "mec_ext":          1,
    "mec_int_long":     2,
    "mec_rda_int":      3,
    "mec_prof":         4,
    "mec_n_grid":       5,
    "mec_pletina":      6,
    "sh_doblado":       0,
    "sh_ext":           1,
    "sh_rda_int":       2,
    "sh_nervios":       3,
    "sh_r_circ_int":    4,
    "sh_soporte":       5,
    "sh_pestanha":      6,
    "mtc_esp_hc":       0,
    "mtc_pest_largo":   1,
    "mtc_pest_ancho":   2
}

genValues = {
    "dist_entre_lados": 200,
    "espesor"         : 3,
    "forma":            "cuadrada",
    "material":         "EN_AW_7075",
    "metrica":          "M3",
    "leyesPredet":      "true",
}

mecValues = {
    "mec_espesor":      4,
    "mec_nervios":      10,
    "mec_ext":          20,
    "mec_int_long":     25,
    "mec_rda_int":      2,
    "mec_prof":         2.5,
    "mec_n_grid":       4,
    "mec_pletina":      10,
}

shValues = {
    "sh_doblado":       4,
    "sh_espesor":       2,
    "sh_ext":           10,
    "sh_rda_int":       1,
    "sh_nervios":       6,
    "sh_r_circ_int":    25,
    "sh_soporte":       10,
    "sh_pestanha":      10,
}

mtcValues = {
    "mtc_esp_hc":       10,
    "mtc_pest_largo":   10,
    "mtc_pest_ancho":   10,
}


# def checkValue(val):
#     try:
#         return str(val).replace(",", ".")
#     except:
#         ValueError("Not a number. Check the entered value.")


def readDesignTable(filename):
    initialValues = {}
    try:
        with open(filename, mode="r") as F:
            table = F.readlines()
            for line in table:
                splited = line.split("\t")
                initialValues.update(dict([(splited[0], splited[1].rstrip('\n'))]))
            F.close()
            # print(initialValues)
            return initialValues
    except FileNotFoundError:
        print(str(FileNotFoundError), " File ", filename, 
        " does not exist. ", dicParamName[paramKey], " not changed.")
        return initialValues

def changeValueInFile(filename, paramKey, paramValue):
    try:
        with open(filename, mode="r") as F:
            table = F.readlines()
            table[dicLine[paramKey]] = \
                dicParamName[paramKey]+"\t"+str(paramValue)+"\n"
            F.close()
            try:
                with open(filename, mode="w") as F:
                    F.writelines(table)
                    F.close()
                    print(filename, ": Success in changing parameter ",
                        dicParamName[paramKey], " to ",
                        str(paramValue), ".")
                    return 0
            except PermissionError:
                print(str(PermissionError), " File ", filename, 
                    " not writable. ", dicParamName[paramKey],
                    " not changed.")
                return -2
    except FileNotFoundError:
        print(str(FileNotFoundError), " File ", filename,
            " does not exist. ", dicParamName[paramKey],
            " not changed.")
        return -1

def write_DISTANCIA_ENTRE_LADOS(x):
    out = changeValueInFile(filenameGen, "dist_entre_lados", x)
    return out
    
def write_ESPESOR(x):
    out = changeValueInFile(filenameGen, "espesor", x)
    return out
    
def write_FORMA(x):
    out = changeValueInFile(filenameGen, "forma", x)
    return out
    
def write_MATERIAL(x):
    out = changeValueInFile(filenameGen, "material", x)
    return out
    
def write_METRICA(x):
    out = changeValueInFile(filenameGen, "metrica", x)
    return out
    
def write_LEYES(x):
    out = changeValueInFile(filenameGen, "leyesPredet", x)
    return out
    
# def write_Set_Mecanizado(default, mecValues):
#     if default:
#         out = changeValueInFile(filenameGen, "leyesPredet", "true")
#     else:
#         out=changeValueInFile(filenameGen, "leyesPredet",  "false")
#         out=changeValueInFile(filenameMec, "mec_nervios",  mecValues["mec_nervios"])
#         out=changeValueInFile(filenameMec, "mec_ext",      mecValues["mec_ext"])
#         out=changeValueInFile(filenameMec, "mec_int_long", mecValues["mec_int_long"])
#         out=changeValueInFile(filenameMec, "mec_rda_int",  mecValues["mec_rda_int"])
#         out=changeValueInFile(filenameMec, "mec_prof",     mecValues["mec_prof"])
#         out=changeValueInFile(filenameMec, "mec_n_grid",   mecValues["mec_n_grid"])
#         out=changeValueInFile(filenameMec, "mec_")
#     return out

# def write_Set_Chapa(default, shValues):
#     if default:
#         out = changeValueInFile(filenameGen, "leyesPredet", "true")
#     else:
#         out=changeValueInFile(filenameGen, "leyesPredet",  "false")
#         out=changeValueInFile(filenameSh , "sh_doblado",   shValues["sh_doblado"])
#         out=changeValueInFile(filenameSh , "sh_ext",       shValues["sh_ext"])
#         out=changeValueInFile(filenameSh , "sh_rda_int",   shValues["sh_rda_int"])
#         out=changeValueInFile(filenameSh , "sh_nervios",   shValues["sh_nervios"])
#         out=changeValueInFile(filenameSh , "sh_r_circ_int",shValues["sh_r_circ_int"])
#         out=changeValueInFile(filenameSh , "sh_soporte",   shValues["sh_soporte"])
#         out=changeValueInFile(filenameSh , "sh_pestanha",  shValues["sh_pestanha"])
#     return out

# def write_Set_MtC(default, mtcValues):
# def write_Set_MtC(mtcValues):
#     # if default:
#     #     out = changeValueInFile(filenameGen, "leyesPredet", "true")
#     # else:
#     #     out=changeValueInFile(filenameGen, "leyesPredet", "false")
#         out=changeValueInFile(filenameCom , "mtc_esp_hc", mtcValues[dicParamName["mtc_esp_hc"]])
#         out=changeValueInFile(filenameCom , "mtc_pest_largo", mtcValues[dicParamName["mtc_pest_largo"]])
#         out=changeValueInFile(filenameCom , "mtc_pest_ancho", mtcValues[dicParamName["mtc_pest_ancho"]])
#         return out

def get_mec_ranges(filenameGen_, filenameMec_):
    genValues_ = readDesignTable(filenameGen_)
    Values_ = readDesignTable(filenameMec_)

    e = float(genValues_[dicParamName["espesor"]])
    DEL = float(genValues_[dicParamName["dist_entre_lados"]])

    Ranges_ = Values_.copy()
    Ranges_[dicParamName["mec_prof"]] = [e*0.00001, e+1]
    Ranges_[dicParamName["mec_ext"]] = [7., 30.]
    Ranges_[dicParamName["mec_rda_int"]] = [0.001, 3.]
    Ranges_[dicParamName["mec_nervios"]] = [7., 30.]
    Ranges_[dicParamName["mec_int_long"]] = [DEL/20, DEL/3]
    Ranges_[dicParamName["mec_n_grid"]] = [1, 8]
    Ranges_[dicParamName["mec_pletina"]] = [8., 50.]

    # print(Ranges_)

    return Ranges_

def get_sh_ranges(filenameGen_, filenameSh_):
    genValues_ = readDesignTable(filenameGen_)
    Values_ = readDesignTable(filenameSh_)

    e = float(genValues_[dicParamName["espesor"]])
    DEL = float(genValues_[dicParamName["dist_entre_lados"]])
    RD = DEL*0.02

    Ranges_ = Values_.copy()
    Ranges_[dicParamName["sh_doblado"]] = [DEL*0.01, DEL*0.03]
    Ranges_[dicParamName["sh_ext"]] = [(RD + e + 1.)*1.05, (RD + e + 1.)*2]
    Ranges_[dicParamName["sh_rda_int"]] = [0.001, 3.]
    Ranges_[dicParamName["sh_nervios"]] = [6., 30.]
    Ranges_[dicParamName["sh_r_circ_int"]] = [DEL/20, DEL/3]
    Ranges_[dicParamName["sh_soporte"]] = [10., 60.]
    Ranges_[dicParamName["sh_pestanha"]] = [6., 30.]

    # print(Ranges_)

    return Ranges_

def get_mtc_ranges(filenameGen_, filenameCom_):
    genValues_ = readDesignTable(filenameGen_)
    Values_ = readDesignTable(filenameCom_)

    # print(genValues_)
    # print(genValues_[dicParamName["espesor"]])
    e = float(genValues_[dicParamName["espesor"]])*6
    pl = 0.67*0.5*float(genValues_[dicParamName["dist_entre_lados"]])
    if genValues_[dicParamName["forma"]] == "hexagonal":
        pl = pl/math.sin(math.pi/3)
    # print(Values_)

    Ranges_ = Values_.copy()
    Ranges_[dicParamName["mtc_esp_hc"]] = [0.5*e, 1.5*e]
    Ranges_[dicParamName["mtc_pest_largo"]] = [0.5*pl, 1.5*pl]
    Ranges_[dicParamName["mtc_pest_ancho"]] = [10., 40.]

    # print(Ranges_)

    return Ranges_

def rango_espesor(caso):
    if caso == "M":
        return RANGO_ESPESOR_M
    elif caso == "S":
        return RANGO_ESPESOR_S
    elif caso == "C":
        return RANGO_ESPESOR_C
    
# def writeParamGen(filenameGen, x)
    
def main(args):
    dist_entre_lados = 200
    forma = FORMA[0]
    material = MATERIAL[1]
    leyes = LEYES[0]
    
    write_DISTANCIA_ENTRE_LADOS(dist_entre_lados)
    write_FORMA(forma)
    write_MATERIAL(material)
    # if material == MATERIAL[0]:
    #     write_Set_Mecanizado(leyes, mecValues)
    # elif material == MATERIAL[1]:
    #     write_Set_Chapa     (leyes, shValues)
    # elif material == MATERIAL[2]:
    #     write_Set_MatComp(leyes)
    print("")

    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
